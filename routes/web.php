<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use Illuminate\Http\Request;
use Illuminate\Http\Response;

$router->get('/', function (Request $request) {
    $md5 = getDateHashUpToMinutes();;

    if ($request->header('if-none-match') === $md5) {
        abort(304);
    }

    return (new Response())
        ->header('cache-control', 'max-age=0, must-revalidate')
        ->header('etag', $md5)
        ->setContent(makePage('Home'));

});

$router->get('/p1', function () {
    return (new Response())
        ->header('cache-control', 'max-age=3600')
        ->setContent(makePage('Page 1'));
});

$router->get('/p2', function () {
    return (new Response())
        ->setContent(makePage('Page 2'));
});

$router->get('/p3', function (Request $request) {
    $md5 = getDateHashUpToMinutes();

    if ($request->header('if-none-match') === $md5) {
        abort(304);
    }

    return (new Response())
        ->header('cache-control', 'max-age=10, must-revalidate')
        ->header('etag', $md5)
        ->setContent(makePage('Page 3'));

});

function makePage(string $title): string
{
    $listItems = '';
    for ($i = 0; $i < 10000; $i++) {
        $listItems .= "<li>$i</li>";
    }
    return '
        <!DOCTYPE html>
        <html lang="en">
        <meta charset="UTF-8">
            <title>Page Title</title>
            <meta name="viewport" content="width=device-width,initial-scale=1">
            <link rel="stylesheet" href="">
            <style>
            </style>
            <script src=""></script>
        <body>

        <ul>
            <li><a href="/">Home - ETag</a></li>
            <li><a href="/p1">Page1 - Cache</a></li>
            <li><a href="/p2">Page2 - No cache</a></li>
            <li><a href="/p3">Page3 - ETag + max-age</a></li>
        </ul>

        <h1>' . $title . '</h1>
        <h2>Cached date: ' . date('Y-m-d H:i:s') . '</h2>

        <ul>
            ' . $listItems . '
        </ul>

        </body>
        </html>
    ';
}

function getDateHashUpToMinutes(): string
{
    return md5(date('Y-m-d H:i'));
}
