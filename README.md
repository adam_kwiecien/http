# This repo shows basic usage of http caching

## Code

Significant code is placed in routes/web.php

## Run mini site

To test on your own run `php -S localhost:8000 -t public`

## Info

This mini app is in fact lumen framework - [https://lumen.laravel.com/](https://lumen.laravel.com/)
